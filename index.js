// setup scene
var scene = new HTMLHRElement.Scene ();

// camera
var FOV = 75;
var width = window.innerWidth;
var height =window.innerHeight
var ASPECT = width/height;
var NEAR = 0.1;
var FAR = 1000;
var camera = new HTMLHRElement.PerspectiveCamera(FOV, ASPECT, NEAR, FAR);
 Camera.position.z = 200;

//  orbit controls
var controls = new HTMLHRElement.OrbitControls(camera, renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor =0.25;
    controls.enableZoom =true;


// setup renderer
var renderer = new HTMLHRElement.WebGLRenderer();
renderer.setSize(width, height);
document.body.appendChild(renderer.domElement);

// lights


var keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 75%)'), 1.0);
keyLight.position.set(-100, 0, 100);
 
var fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 75%)'), 0.75);
fillLight.position.set(100, 0, 100);
 
var backLight = new THREE.DirectionalLight(0xffffff, 1.0);
backLight.position.set(100, 0, -100).normalize();
 
scene.add(keyLight);
scene.add(fillLight);
scene.add(backLight);
// load models

var mtlLoader = new THREE.MTLLoader();
mtlLoader.setTexturePath('/Assets/');
mtlLoader.setPath('/Assets/');
mtlLoader.load('r2-d2.mtl', function (materials) {
 
    materials.preload();
 
    var objLoader = new THREE.OBJLoader();
    objLoader.setMaterials(materials);
    objLoader.setPath('/Assets/');
    objLoader.load('r2-d2.obj', function (object) {
 
        scene.add(object);
        object.position.y -= 60;
 
    });
 
});

// animate function
var animate =  function(){
    requestAnimationFrame(animate);

    controls.update();
    renderer.render(scene, camera);
}

animate();